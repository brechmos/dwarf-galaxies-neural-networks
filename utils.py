from decimal import Decimal
import itertools
import math
import numpy as np

def display_image(position):
    image = train['features'][position].squeeze()
    minim, maxim = np.min(image), np.max(image)
    image = (image - minim) / (maxim - minim)
    plt.figure(1)
    plt.clf()
    plt.title('Example {}. Label: {}'.format(position, train['labels'][position]))
    plt.imshow(image[:,:,:3].astype(np.float64), cmap=plt.cm.gray_r)


def scale_column(column_scaling, column, value):
    """
    Scale the data to be between 0 and 1. Clipping might be necessary if 
    the min and max are determined using percentiles.
    """
    vmin, vmax = column_scaling[column]['min'], column_scaling[column]['max']
    return np.clip((value - vmin) / (vmax - vmin), 0, 1)


def fexp(number):
    """
    Get the exponent of a number.
    """
    (sign, digits, exponent) = Decimal(number).as_tuple()
    return len(digits) + exponent - 1


def roundup(number):
    """
    Round the number up to the nearest factor, but in a nice way
    """
    theexp = fexp(number)

    if theexp < 0:
        return math.ceil(number * 10**abs(theexp)) / (10**abs(theexp))
    else:
        return math.ceil(number * 10**abs(theexp-2)) / (10**abs(theexp-2))
    return
vroundup = np.vectorize(roundup)

def rounddown(number):
    """
    Round down to the nearest factor, but in a nice way
    """
    theexp = fexp(number)

    if theexp < 0:
        return math.floor(number / 10**abs(theexp)) * (10**abs(theexp))
    else:
        return math.floor(number * 10**abs(theexp-2)) / (10**abs(theexp-2))
    return
vrounddown = np.vectorize(rounddown)


def random_subsets(thelist, subsets, probabilities=None):
    """
    Create random subset of elements from thelist.  

    Use the probabilities if not None, otherwise a uniform probability
    for each element of thelist.
    """
    used = []

    if probabilities is None:
        probabilities = np.ones(len(thelist))


    for subset in subsets:
        all_used = list(itertools.chain(*used))

        possible = list(set(thelist) - set(all_used))

        probabilities_subset = probabilities[possible]
        probabilities_subset = probabilities_subset / np.sum(probabilities_subset)

        thesublist = np.random.choice(possible, subset, replace=False, p=probabilities_subset)

        used.append(thesublist)

    return used



def image_generator(dwarfs_table, row_indices,
                    columns,
                    get_input, get_output,
                    batch_size=64):
    """
    Need to pass in dwarfs_table
    indices over which we will do a random choice

    Methods must be defined:
      get_input(filename)
      get_output(row, columns)
    """
    while True:
        # Select files (paths/indices) for the batch
        batch_dwarfs_rows = np.random.choice(a=dwarfs_table[row_indices], size=batch_size, replace=False)

        batch_input = []
        batch_output = []

        # Read in each input, perform preprocessing and get labels
        for row in batch_dwarfs_rows:
            data = get_input(row['filename'])
            output = get_output(row, columns)

            #input = preprocess_input(image=input)
            batch_input += [ data ]
            batch_output += [ output ]

        # Return a tuple of (input,output) to feed the network
        batch_x = np.array( batch_input )
        batch_y = np.array( batch_output )

        #batch_x = vgg19_preprocess_input(batch_x)
        #batch_x = (batch_x - np.min(batch_x)) / (np.max(batch_x) - np.min(batch_x))

        yield batch_x, batch_y


